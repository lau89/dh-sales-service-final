package com.dh.fullstack.dhsaleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DhSaleServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DhSaleServiceApplication.class, args);
    }

}
